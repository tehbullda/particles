#include "stdafx.h"
#include "Randomizer.hpp"
namespace particles {
	namespace Randomizer
	{
		std::random_device rd;
		std::mt19937 engine(rd());

		int Randomizer::GetRandomInt(int p_min, int p_max) {
			int min = p_min < p_max ? p_min : p_max;
			int max = p_min < p_max ? p_max : p_min;

			std::uniform_int_distribution<int> distr(min, max);
			return distr(engine);
		}

		double Randomizer::GetRandomDouble(double p_min, double p_max) {
			double min = p_min < p_max ? p_min : p_max;
			double max = p_min < p_max ? p_max : p_min;

			std::uniform_real_distribution<double> distr(min, max);
			return distr(engine);
		}
		float Randomizer::GetRandomFloat(float p_min, float p_max) {
			float min = p_min < p_max ? p_min : p_max;
			float max = p_min < p_max ? p_max : p_min;

			std::uniform_real_distribution<float> distr(min, max);
			return distr(engine);
		}
	}
}