#pragma once

#include "SFML\System.hpp"
#include "SFML\Window.hpp"
#include "SFML\Graphics.hpp"

#include "ServiceLocator.hpp"


namespace particles
{
	class Particle
	{
	public:
		enum  ForceMode
		{
			VelocityChange,
			Acceleration
		};
		Particle();
		Particle(sf::Texture& texture);
		Particle(const float& radius);
		Particle(const sf::Vector2f& size);

		~Particle();

		void SetPosition(const sf::Vector2f& pos);
		void SetRotation(const float& rot);
		void SetDirection(const sf::Vector2f& dir);
		void SetColor(const sf::Color& col);
		void SetLifetime(const float& time);
		void SetVelocity(const float& speed);
		void SetFriction(const float& fric);
		void SetScale(const float& scale);
		void UseLocalPosition(const bool& useLocal);

		void SetColorOverLifetime(const sf::Color& startColor, const sf::Color& endColor);
		void SetScaleOverLifetime(const float& start, const float& end);
		void SetRotationOverLifetime(const float& min);
		


		sf::Vector2f GetPosition();
		sf::Vector2f GetDirection();
		sf::Color GetColor();
		float GetLifetime();
		float GetVelocity();
		float GetFriction();


	private:
		bool Initialize();
	public:
		bool Update(const float& deltatime);
		bool Draw(sf::RenderWindow* window);

	private:
		void UpdateColorOverLifetime(const float& deltatime);
		void UpdateScaleOverLifetime(const float& deltatime);
		void UpdateRotationOverLifetime(const float& deltatime);
		

	public:
		void AddForce(const sf::Vector2f& force, const ForceMode& forceMode);
		//void AddForceInDirection(const float& force);


	private:
		sf::Vector2f GetNewPosition();
		sf::Vector2f CalculateFriction(const sf::Vector2f& velocity);
		sf::Sprite* SetOrigin(sf::Sprite* object);
		sf::RectangleShape* SetOrigin(sf::RectangleShape* object);
		sf::CircleShape* SetOrigin(sf::CircleShape* object);

	private:
		
		sf::Sprite* CreateSprite(sf::Texture& texture);
		sf::RectangleShape* CreateRectangleShape(const sf::Vector2f& size);
		sf::CircleShape* CreateCircle(const float& radius);

		bool LifetimeLeft(const float& deltatime);

	private:
		bool useLocalPosition;

		sf::Sprite* sprite;
		sf::RectangleShape* rect;
		sf::CircleShape* circle;

		sf::Vector2f emitterPosition;
		sf::Vector2f position;
		sf::Vector2f direction;
		sf::Color color;

		float rotation;
		float lifetime;
		float velocity;
		float friction;
		float particleScale;

		sf::Vector2f velocityChange;

	private:
		bool colorOverLifetime;
		sf::Color startColor;
		sf::Color endColor;

		bool scaleOverLifeTime;
		float startScale;
		float endScale;

		float rotationOverLifetime;
	private:
		float timer;


	};
}