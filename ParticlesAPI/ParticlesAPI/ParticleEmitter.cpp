#include "stdafx.h"
#include "ParticleEmitter.hpp"
#include "Randomizer.hpp"
#include "Math.hpp"
#include "ServiceLocator.hpp"
#include <vector>

using namespace particles;
bool Approx(float a, float b) {
	return fabs(a - b) < 0.000001f;
}
ParticleEmitter::ParticleEmitter() {
	dispersiondegrees = 360.0f;
	finished = false;
	emitting = true;
	looping = false;
	useColorOverLifetime = false;
	useScaleOverLifetime = false;
	useRandomStartRotation = false;
	useRotationOverLifetime = false;
	useRandomLifetime = false;
	emittertag = "";
	currentEmissiontime = currentdowntime = 0.0f;
	timebetweenemissions = 0.1f;
	dispersiondegrees = 360.0f;
	emissionRate = 60;
	particlelifetime = 1.0f;
	looping = true;
	direction = sf::Vector2f(0, 1);
	radius = 5.0f;
	minEmissionRadius = 0.0f;
	maxEmissionRadius = 0.0f;
}

ParticleEmitter::~ParticleEmitter() {
	Destroy();
}

//void ParticleEmitter::CreateParticles(ParticleType type, sf::Vector2f originpoint, float emissiontime, float lifetime, uint32_t emissionPerSecond,
//	sf::Vector2f direction, sf::Texture *texture, float radius, float dispersiondegrees, bool emitImmediately, std::string tag) {
//
//	EmissionJob *newjob = new EmissionJob();
//	newjob->type = type;
//	newjob->emissiontime = emissiontime;
//	newjob->lifetime = lifetime;
//	newjob->emissionRate = emissionPerSecond;
//	newjob->originPoint = originpoint;
//	newjob->direction = direction;
//	newjob->texture = texture;
//	newjob->radius = radius;
//	newjob->dispersiondegrees = dispersiondegrees;
//	newjob->currentEmissiontime = newjob->currentdowntime = 0.0f;
//	newjob->timebetweenemissions = 1.0f / emissionPerSecond;
//	newjob->finished = false;
//	newjob->emitting = emitImmediately;
//	newjob->tag = tag;
//	activeJobs.push_back(newjob);
//}

void ParticleEmitter::Update(const float &deltatime) {
	UpdateTimers(deltatime);
	if (emitting) {
		AddParticles(deltatime);
	}
	ApplyGravitationalForces();
	for (unsigned int i = 0; i < activeParticles.size(); ++i) {
		if (!activeParticles[i]->Update(deltatime)) {
			DestroyParticle(i);
		}
		/*for (unsigned int j = i; j < activeParticles.size(); ++j) {
			if (i == j) continue;
			if (Math::Distance(activeParticles[i]->GetPosition(), activeParticles[j]->GetPosition()) < 1000.0f) {
				sf::Vertex line[] = {
					sf::Vertex(activeParticles[i]->GetPosition()),
					sf::Vertex(activeParticles[j]->GetPosition())
				};
				app::ServiceLocator<app::Window>().get_service()->GetRenderWindow()->draw(line, 2, sf::Lines);
			}
		}*/
	}
}
void ParticleEmitter::UpdateTimers(const float &deltatime) {
	if (looping) {
		return;
	}
	currentEmissiontime += deltatime;
	if (currentEmissiontime > emissiontime) {
		emitting = false;
	}
}

void ParticleEmitter::AddForce(sf::Vector2f pos, particles::Particle::ForceMode mode, float range, float strength) {
	for (unsigned int i = 0; i < activeParticles.size(); ++i) {
		float distance = particles::Math::Distance(activeParticles[i]->GetPosition(), pos);
		if (distance < range)
		{
			float multiplier = distance / range > 1.0f ? 1.0f : distance / range; // Make it stronger if the particle is closer to force, but cap it at 1.0f to not make it ridiculous
			activeParticles[i]->AddForce((activeParticles[i]->GetPosition() - pos) * strength, mode);
		}
	}
}

void ParticleEmitter::Destroy() {
	for (unsigned int i = 0; i < activeParticles.size(); ++i) {
		delete activeParticles[i];
		activeParticles[i] = nullptr;
	}
	activeParticles.clear();
}
void ParticleEmitter::DestroyAllActiveParticles() {
	for (unsigned int i = 0; i < activeParticles.size(); ++i) {
		DestroyParticle(i);
	}
	activeParticles.clear();
}

void ParticleEmitter::Draw(sf::RenderWindow *window) {
	for (unsigned int i = 0; i < activeParticles.size(); ++i) {
		activeParticles[i]->Draw(window);
	}
}


void ParticleEmitter::AddParticles(const float &deltatime) {
	currentdowntime += deltatime;
	int newParticleCount = 0;
	if (currentdowntime > timebetweenemissions) {
		if (Approx(timebetweenemissions, 0.0f)) {
			newParticleCount = 5; // So it doesn't break if timebetweenemissions is 0 for some reason.
		}
		else {
			newParticleCount = currentdowntime / timebetweenemissions;
		}
		currentdowntime = 0.0f;
	}
	for (unsigned int i = 0; i < newParticleCount; ++i) {
		Particle *newparticle;
		switch (type) {
		case ParticleType::Texture:
			newparticle = new Particle(*texture);
			break;
		case ParticleType::Circle:
			newparticle = new Particle(radius);
			break;
		case ParticleType::Rect:
			newparticle = new Particle(rectSize);
			break;
		default:
			newparticle = new Particle();
			break;
		}
		sf::Vector2f dir;
		dir = GetRandomDir(direction, dispersiondegrees);
		if (minEmissionRadius == 0.0f && maxEmissionRadius == 0.0f)
			newparticle->SetPosition(originPoint);
		else
			newparticle->SetPosition(originPoint + dir * Randomizer::GetRandomFloat(minEmissionRadius, maxEmissionRadius));



		newparticle->AddForce(dir, Particle::ForceMode::VelocityChange);
		if (useRandomLifetime)
			newparticle->SetLifetime(Randomizer::GetRandomFloat(minLifetime, maxLifetime));
		else
			newparticle->SetLifetime(particlelifetime);

		if (useRandomStartRotation)
			newparticle->SetRotation(Randomizer::GetRandomFloat(minRandomRot, maxRandomRot));
		else
			newparticle->SetRotation(rotation);

		if (useScaleOverLifetime)
			newparticle->SetScaleOverLifetime(startScale, endScale);
		if (useColorOverLifetime)
			newparticle->SetColorOverLifetime(startColor, endColor);

		if (useRotationOverLifetime)
			newparticle->SetRotationOverLifetime(Randomizer::GetRandomFloat(minLifetimeRotation, maxLifetimeRotation));

		newparticle->SetVelocity(0.5f);


		activeParticles.push_back(newparticle);
	}
}
void ParticleEmitter::ApplyGravitationalForces() {
	for (unsigned int i = 0; i < gravitationalPoints.size(); ++i) {
		AddForce(gravitationalPoints[i].position, Particle::ForceMode::Acceleration, gravitationalPoints[i].range, gravitationalPoints[i].strength);
	}
}
void ParticleEmitter::AddGravitationalPoint(const sf::Vector2f &pos, const float &range, const float &strength) {
	gravitationalPoints.push_back(GravitationalPoint(pos, strength, range));
}
void ParticleEmitter::RemoveAllGravitationalPoints() {
	gravitationalPoints.clear();
}
void ParticleEmitter::UpdateTimeBetweenEmissions() {
	timebetweenemissions = 1.0f / (float)emissionRate;
}

void ParticleEmitter::DestroyParticle(const int &index) {
	delete activeParticles[index];
	activeParticles[index] = nullptr;
	activeParticles.erase(activeParticles.begin() + index);
}
sf::Vector2f ParticleEmitter::GetRandomDir(sf::Vector2f reference, float deviantCone) {
	//if (Approx(fmod(deviantCone, 180.1f), 180.0f)) {//fucking floats 
	//	return particles::Math::Normalize(sf::Vector2f(particles::Randomizer::GetRandomFloat(-1, 1), particles::Randomizer::GetRandomFloat(-1, 1)));
	//}

	reference = Math::Normalize(reference);
	sf::Vector2f def = sf::Vector2f(0, 1);
	float dot = def.x*reference.x + def.y*reference.y;      // dot product
	float det = def.x*reference.y - def.y*reference.x;      // determinant
	float angle = atan2(det, dot);
	float angledeg = fabs(angle * (180.0f / 3.1415f));
	angledeg += Randomizer::GetRandomFloat(-deviantCone, deviantCone);
	angle = angledeg * (3.1415f / 180.0f);
	float cs = cos(angle);
	float sn = sin(angle);
	float px = reference.x * cs - reference.y * sn;
	float py = reference.x * sn + reference.y * cs;
	reference = sf::Vector2f(px, py);
	return reference;
}

