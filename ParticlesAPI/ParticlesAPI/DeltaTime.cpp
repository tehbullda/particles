#include "stdafx.h"

#include "DeltaTime.hpp"

using namespace app;

DeltaTime::DeltaTime()
{
	Update();
};
DeltaTime::~DeltaTime()
{

};

DeltaTime::Ptr DeltaTime::Create()
{
	return DeltaTime::Ptr(new DeltaTime());
}

float DeltaTime::getDeltaTime()
{
	return fDeltaTime;
};

void DeltaTime::Update()
{
	m_Time = m_Clock.restart();
	fDeltaTime = m_Time.asSeconds();
	int bob = 0;
};

void DeltaTime::setDeltaTime(const float& p_Time)
{
	fDeltaTime = p_Time;
};
