#pragma once

#include "stdafx.h"

namespace app
{
	class Window
	{
	private:
		Window(const std::string & windowName, const unsigned& style, const unsigned & width, const unsigned & height, const int & fpsCap);
	public:
		~Window();

		typedef std::unique_ptr<Window> Ptr;
		static Ptr Create(const std::string & windowName, const unsigned& style, const unsigned & width, const unsigned & height, const int & fpsCap);




		bool Display();
		void Exit();
		void Clear();


		void SetClearColor(const sf::Color& clearColor);
		

		sf::RenderWindow* GetRenderWindow();



	private:
		sf::RenderWindow* window;
		sf::Color clearColor;


	};
};
