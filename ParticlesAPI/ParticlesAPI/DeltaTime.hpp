#pragma once

#include "stdafx.h"

namespace app
{
	static float fDeltaTime;

	class DeltaTime
	{
	public:
		DeltaTime();
		~DeltaTime();

		typedef std::unique_ptr<DeltaTime> Ptr;
		static Ptr Create();

		void Update();

		static float getDeltaTime();

		static void setDeltaTime(const float& p_Time);
	private:
		sf::Clock m_Clock;
		sf::Time m_Time;
	};
}