#pragma once
#include "stdafx.h"
#include "Window.hpp"
#include "Input.hpp"
#include "DeltaTime.hpp"
#include "Particle.hpp"
#include "ParticleEmitter.hpp"

namespace app
{
	class Engine
	{
	public:
		Engine();
		~Engine();

		bool Initialize();
		bool Run();
		bool Draw();
		bool CleanUp();
	
		static void Shutdown();

		bool IsRunning();
	

	private:
		Window::Ptr window;
		Input::Ptr input;
		particles::ParticleEmitter *showCaseEmitter_red;
		particles::ParticleEmitter *showCaseEmitter_blue;

		DeltaTime::Ptr deltaTime;
		static bool running;
		particles::Particle* testParticle;

		int currentShowcase;
		float showcaseRedDegrees;
	};

}
