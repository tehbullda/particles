// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

#include <memory>

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <cmath>

#include "Engine.hpp"
#include "Debug.hpp"


// TODO: reference additional headers your program requires here
