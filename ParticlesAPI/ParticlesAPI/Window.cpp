#pragma once

#include "stdafx.h"
#include "Window.hpp"

namespace app
{

	Window::Window(const std::string& name, const unsigned& style, const unsigned& width, const unsigned& height, const int& fpsCap)
	{
		window = new sf::RenderWindow(sf::VideoMode(width, height), name, style);
		window->setFramerateLimit(fpsCap);

		clearColor = sf::Color::White;
		Clear();
	};

	Window::~Window()
	{

	};

	Window::Ptr Window::Create(const std::string & windowName, const unsigned& style, const unsigned & width, const unsigned & height, const int & fpsCap)
	{
		return Window::Ptr(new Window(windowName, style, width, height, fpsCap));
	};

	bool Window::Display()
	{
		window->display();
		return true;
	};

	void Window::Exit()
	{

	};

	void Window::Clear()
	{
		window->clear(clearColor);
	};

	void Window::SetClearColor(const sf::Color& color)
	{
		clearColor = color;
	};

	sf::RenderWindow* Window::GetRenderWindow()
	{
		return window;
	};

}
