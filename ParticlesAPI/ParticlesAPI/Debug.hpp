#pragma once


#include "stdafx.h"

namespace app
{
	class Debug
	{
	public:
		static enum DebugLevel
		{
			Info,
			Error,
			Fatal
		};


		Debug();
		~Debug();

		static void Log(const DebugLevel& logDebugLevel, const std::string& text);
		static void Log(const std::string& text);
		static void SetDebugLevel(const DebugLevel& level);
	private:
		
		static bool ShouldLog(const DebugLevel& logDebugLevel);
	
		static DebugLevel debugLevel;

	};

}