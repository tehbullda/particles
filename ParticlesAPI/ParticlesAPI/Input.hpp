#pragma once


#include "stdafx.h"
#include "Window.hpp"



namespace app
{
	class Input
	{
	private:
		Input();
	public:
		~Input();

		typedef std::unique_ptr<Input> Ptr;
		static Ptr Create();

		void initialize();
		void update();

		bool getKey(const int& p_Key);
		bool getKeyDown(const int& p_Key);
		bool getKeyReleased(const int& p_Key);

		sf::Vector2f getMousePos();
		bool getMouse(const int& p_Key);
		bool getMouseDown(const int& p_Key);
		bool getMouseReleased(const int& p_Key);

		bool getAnyKey();

	private:
		void setAll(const bool& p_All);
		void checkKeys();
		void checkMousePos();

		Window* m_Window;
		sf::Vector2i m_MousePos;
		bool m_Keys[100];
		bool m_PrevKeys[100];
		bool m_MouseKeys[5];
		bool m_PrevMouseKeys[5];
	};
}
