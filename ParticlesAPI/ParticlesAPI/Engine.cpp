#include "stdafx.h"
#include "Window.hpp"
#include "Engine.hpp"
#include "ServiceLocator.hpp"
#include "ParticleEmitter.hpp"
#include "Particle.hpp"
#include "Randomizer.hpp"
#include "Math.hpp"

namespace app
{
	bool Engine::running = true;

	Engine::Engine()
	{

	};
	Engine::~Engine()
	{

	};

	bool Engine::Initialize()
	{
		window = Window::Create("TestApplicationForParticles", sf::Style::Default, 1280, 720, 60);
		ServiceLocator<Window>().set_service(window.get());
		window->SetClearColor(sf::Color(15, 15, 15, 255));

		showCaseEmitter_red = new particles::ParticleEmitter();
		showCaseEmitter_red->SetType(particles::ParticleType::Rect);
		showCaseEmitter_red->SetOriginPoint(sf::Vector2f(window->GetRenderWindow()->getSize().x / 2, window->GetRenderWindow()->getSize().y / 2));
		showCaseEmitter_red->SetParticleLifetime(3.0f);
		showCaseEmitter_red->SetEmissionRate(100);
		showCaseEmitter_red->SetEmissionTime(3.0f);
		showCaseEmitter_red->SetRadius(7.0f);
		showCaseEmitter_red->SetDirection(sf::Vector2f(particles::Randomizer::GetRandomFloat(-5, 5), particles::Randomizer::GetRandomFloat(-5, 5)));
		showCaseEmitter_red->SetRectSize(sf::Vector2f(5, 5));
		showCaseEmitter_red->SetStartRotation(45);
		//showCaseEmitter_red->SetDirection(sf::Vector2f(particles::Randomizer::GetRandomFloat(-5, 5), particles::Randomizer::GetRandomFloat(-5, 5)));
		showCaseEmitter_red->SetColorOverLifetime(sf::Color::Red, sf::Color(255, 0, 0, 0));
		showCaseEmitter_red->SetScaleOverLifetime(1.0f, 5.0f);
		showCaseEmitter_red->SetDispersionDegrees(1);
		showCaseEmitter_red->SetEmitting(true);
		showCaseEmitter_red->SetEmissionRadius(500.0f, 500.0f);
		showCaseEmitter_red->SetRectSize(sf::Vector2f(5, 5));


		showCaseEmitter_blue = new particles::ParticleEmitter();
		showCaseEmitter_blue->SetType(particles::ParticleType::Circle);
		showCaseEmitter_blue->SetOriginPoint(sf::Vector2f(window->GetRenderWindow()->getSize().x / 2, window->GetRenderWindow()->getSize().y / 2));
		//showCaseEmitter_blue->SetColorOverLifetime(sf::Color::Blue, sf::Color(0, 0, 255, 0));
		showCaseEmitter_blue->SetDirection(sf::Vector2f(particles::Randomizer::GetRandomFloat(-5, 5), particles::Randomizer::GetRandomFloat(-5, 5)));
		//showCaseEmitter_blue->SetScaleOverLifetime(0.5, 2);
		showCaseEmitter_blue->SetEmissionRate(250);
		showCaseEmitter_blue->SetEmitting(true);








		input = Input::Create();
		ServiceLocator<Input>().set_service(input.get());
		input->initialize();

		deltaTime = DeltaTime::Create();




		/*testParticle = new particles::Particle(sf::Vector2f(10, 10));
		testParticle->SetColor(sf::Color::Red);
		testParticle->SetPosition(sf::Vector2f(100, 100));
		testParticle->SetFriction(1);
		testParticle->SetLifetime(5.0f);
		testParticle->SetRotation(45);*/

		return true;
	};
	bool Engine::Run()
	{
		window->Clear();
		deltaTime->Update();
		input->update();


		if (currentShowcase == 0) {
			//showCaseEmitter_red->SetDispersionDegrees(showcaseRedDegrees);
			showCaseEmitter_red->SetEmissionRadius(showcaseRedDegrees, showcaseRedDegrees);
			showcaseRedDegrees += deltaTime->getDeltaTime() * 50;
			if (showcaseRedDegrees >= 360)
				showcaseRedDegrees = 0.0f;

			showCaseEmitter_red->Update(deltaTime->getDeltaTime());
		}
		if (currentShowcase == 1)
			showCaseEmitter_blue->Update(deltaTime->getDeltaTime());

		if (input->getKeyDown(sf::Keyboard::Space))
		{
			++currentShowcase;
		}
		//++currentShowcase;
		if (currentShowcase > 1)
			currentShowcase = 0;

		if (input->getMouseDown(1)) {
			showCaseEmitter_blue->AddGravitationalPoint(input->getMousePos(), 1000.0f, 1.0f);
		}
		//Debug::Log(std::to_string(currentShowcase));

		return true;
	};
	bool Engine::Draw()
	{
		if (currentShowcase == 0)
			showCaseEmitter_red->Draw(window->GetRenderWindow());
		if (currentShowcase == 1)
			showCaseEmitter_blue->Draw(window->GetRenderWindow());
		//testParticle->Draw();
		window->Display();
		return true;
	};
	bool Engine::CleanUp()
	{
		return true;
	};

	void Engine::Shutdown()
	{
		running = false;
	};
	bool Engine::IsRunning()
	{
		return running;
	};

}