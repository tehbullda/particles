#pragma once

#include "Particle.hpp"

namespace particles {
	struct GravitationalPoint {
		GravitationalPoint(const sf::Vector2f pos, const float &str, const float &rng) {
			position = pos;
			strength = str;
			range = rng;
		}
		sf::Vector2f position;
		float strength;
		float range;
	};
	enum ParticleType {
		Texture,
		Circle,
		Rect
	};
	class ParticleEmitter {
	public:
		ParticleEmitter();
		~ParticleEmitter();

		//void CreateParticles(ParticleType type, sf::Vector2f originpoint, float emissiontime, float lifetime, uint32_t emissionPerSecond, sf::Vector2f direction, sf::Texture *texture = nullptr, float radius = 0.0f, float dispersiondegrees = 360.0f, bool emitImmediately = true, std::string tag = "");
		void Update(const float &deltatime);
		void AddForce(sf::Vector2f pos, particles::Particle::ForceMode mode, float range, float strength = 1.0f);
		void Play() { emitting = true; currentEmissiontime = 0.0f; }
		void Stop() { emitting = false; }
		void ToggleEmission() { emitting = !emitting; }
		void Destroy();
		void DestroyAllActiveParticles();
		void Draw(sf::RenderWindow *window);

		void SetType(const ParticleType &particletype) { type = particletype; }
		void SetEmissionRate(const unsigned int &rate) { emissionRate = rate; UpdateTimeBetweenEmissions(); }
		void SetOriginPoint(const sf::Vector2f &pos) { originPoint = pos; }
		void SetDirection(const sf::Vector2f &dir) { direction = dir; }
		void SetTexture(sf::Texture *tex) { texture = tex; }
		void SetRadius(const float &rad) { radius = rad; }
		void SetRectSize(const sf::Vector2f &rect) { rectSize = rect; }
		void SetDispersionDegrees(const float &degrees) { dispersiondegrees = fmod(degrees/2.0f, 180.1f); }
		void SetEmissionTime(const float &time) { emissiontime = time; }
		void SetParticleLifetime(const float &lifetime) { particlelifetime = lifetime; }
		void SetParticleLifetime(const float& max, const float& min) { useRandomLifetime = true; minLifetime = max; maxLifetime = min; }
		void SetEmitting(const bool &status) { emitting = status; }
		void SetTag(const std::string &tag) { emittertag = tag; }
		void SetLooping(const bool &status) { looping = status; }
		void SetColorOverLifetime(const sf::Color& start, const sf::Color end) { startColor = start; endColor = end; useColorOverLifetime = true; };
		void SetScaleOverLifetime(const float& start, const float& end) { startScale = start; endScale = end; useScaleOverLifetime = true; };
		void SetRotationOverLifetime(const float& min, const float& max) { useRotationOverLifetime = true, maxLifetimeRotation = min, maxLifetimeRotation = max; };
		void SetStartRotation(const float& rot) { rotation = rot; };
		void SetStartRotation(const float& min, const float& max) { useRandomStartRotation = true; minRandomRot = min; maxRandomRot = max; };
		void SetEmissionRadius(const float& min, const float& max) { minEmissionRadius = min; maxEmissionRadius = max; };
		void SetEmittFromEdge(const bool& value) {
		}

		ParticleType GetType() { return type; }
		unsigned int GetEmissionRate() { return emissionRate; }
		sf::Vector2f GetOriginPoint() { return originPoint; }
		sf::Vector2f GetDirection() { return direction; }
		sf::Texture* GetTexture() { return texture != nullptr ? texture : nullptr; }
		float GetRadius() { return radius; }
		sf::Vector2f GetRectSize() { return rectSize; }
		float GetDispersionDegrees() { return dispersiondegrees; }
		float GetEmissionTime() { return emissiontime; }
		float GetParticleLifetime() { return particlelifetime; }
		float GetCurrentEmissionTime() { return currentEmissiontime; }
		std::string GetTag() { return emittertag; }

		bool IsFinished() { return finished; }
		bool IsEmitting() { return emitting; }
		bool IsLooping() { return looping; }

		int GetCurrentlyActiveCount() { return activeParticles.size(); }
		std::vector<Particle*>* GetCurrentlyActiveParticles() { return &activeParticles; }

		void AddGravitationalPoint(const sf::Vector2f &pos, const float &range, const float &strength);
		void RemoveAllGravitationalPoints();

	private:
		void UpdateTimers(const float &deltatime);
		void ApplyGravitationalForces();
		void AddParticles(const float &deltatime);
		void UpdateTimeBetweenEmissions();
		void DestroyParticle(const int &index);
		sf::Vector2f GetRandomDir(sf::Vector2f reference, float deviantCone);
	private:
		std::vector<Particle*> activeParticles;
		std::vector<GravitationalPoint> gravitationalPoints;
		bool looping;
		ParticleType type;
		unsigned int emissionRate;
		sf::Vector2f originPoint;
		sf::Vector2f direction;
		sf::Texture *texture;
		sf::Vector2f rectSize;
		float radius;
		float dispersiondegrees;
		float emissiontime;
		float particlelifetime;
		float currentEmissiontime;
		float timebetweenemissions;
		float currentdowntime;

		bool finished, emitting;
		std::string emittertag;

		float rotation;
		bool useRandomStartRotation;
		float minRandomRot;
		float maxRandomRot;

		bool useRotationOverLifetime;
		float lifetimeRotation;
		float minLifetimeRotation;
		float maxLifetimeRotation;


		bool useColorOverLifetime;
		sf::Color startColor;
		sf::Color endColor;

		bool useScaleOverLifetime;
		float startScale;
		float endScale;

		bool useRandomLifetime;
		float maxLifetime;
		float minLifetime;

		float minEmissionRadius;
		float maxEmissionRadius;
	};
}
