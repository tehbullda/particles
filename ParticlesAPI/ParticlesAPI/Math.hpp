#pragma once
#include "stdafx.h"

namespace particles
{
	class Math
	{
	public:
		static sf::Vector2f Normalize(const sf::Vector2f& vec)
		{
			sf::Vector2f vector;
			float length = Length(vec);
			if (length != 0.0f)
			{
				vector.x = vec.x / length;
				vector.y = vec.y / length;
			}
			return vector;
		}
		static float Length(const sf::Vector2f& vec)
		{
			return sqrt(vec.x * vec.x + vec.y * vec.y);
		}
		static float Distance(const sf::Vector2f& a, const sf::Vector2f& b)
		{
			return Length(sf::Vector2f(a.x - b.x, a.y - b.y));
		}
	};
}