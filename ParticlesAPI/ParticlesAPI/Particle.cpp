#include "stdafx.h"
#include "Particle.hpp"


namespace particles
{
	Particle::Particle()
	{

	};
	Particle::Particle(sf::Texture& texture)
	{
		Initialize();
		sprite = CreateSprite(texture);
	};

	Particle::Particle(const float& radius)
	{
		Initialize();
		circle = CreateCircle(radius);
	};

	Particle::Particle(const sf::Vector2f& size)
	{
		Initialize();
		rect = CreateRectangleShape(size);
	};

	Particle::~Particle()
	{
		if (rect) {
			delete rect;
			rect = nullptr;
		}
		if (circle) {
			delete circle;
			circle = nullptr;
		}
		if (sprite) {
			delete sprite;
			sprite = nullptr;
		}
	};

	bool Particle::Initialize()
	{
		friction = 1.0f;
		colorOverLifetime = false;
		scaleOverLifeTime = false;
		useLocalPosition = false;
		timer = 0.0f;
		return true;
	};

	bool Particle::Update(const float& deltatime)
	{
		SetPosition(GetNewPosition());
		if (colorOverLifetime)
			UpdateColorOverLifetime(deltatime);
		if (scaleOverLifeTime)
			UpdateScaleOverLifetime(deltatime);
		if (rotationOverLifetime)
			UpdateRotationOverLifetime(deltatime);

		return LifetimeLeft(deltatime);
	};

	bool Particle::Draw(sf::RenderWindow* window)
	{
		if (sprite)
			window->draw(*sprite);
		if (rect)
			window->draw(*rect);
		if (circle)
			window->draw(*circle);
		return true;
	};

	void Particle::UpdateColorOverLifetime(const float& deltatime)
	{
		float left = timer / lifetime;
		sf::Color newColor;
		newColor.r = (startColor.r * (1.0f - left)) + (endColor.r * left);
		newColor.g = (startColor.g * (1.0f - left)) + (endColor.g * left);
		newColor.b = (startColor.b * (1.0f - left)) + (endColor.b * left);
		newColor.a = (startColor.a * (1.0f - left)) + (endColor.a * left);
		SetColor(newColor);
	}
	void Particle::UpdateScaleOverLifetime(const float & deltatime)
	{
		float left = timer / lifetime;
		SetScale((startScale * (1.0f - left)) + (endScale * left));
	}
	void Particle::UpdateRotationOverLifetime(const float & deltatime)
	{
		rect->rotate(rotationOverLifetime * deltatime);
	};

	void Particle::AddForce(const sf::Vector2f& force, const ForceMode& forceMode)
	{
		if (forceMode == ForceMode::VelocityChange) {
			velocityChange += force;
		}
		if (forceMode == ForceMode::Acceleration) {
			velocityChange.x *= force.x/* * app::DeltaTime::getDeltaTime()*/;
			velocityChange.y *= force.y /** app::DeltaTime::getDeltaTime()*/;
		}
	};
	//void Particle::AddForceInDirection(const float& force)
	//{
	//	velocityChange += direction * force;
	//};
	sf::Vector2f Particle::GetNewPosition()
	{
		return  position + CalculateFriction(velocityChange);
	};

	sf::Vector2f Particle::CalculateFriction(const sf::Vector2f& velocity)
	{
		return velocity * friction;
	}
	sf::Sprite* Particle::SetOrigin(sf::Sprite * object)
	{
		object->setOrigin(object->getGlobalBounds().width / 2, object->getGlobalBounds().height / 2);
		return object;
	};
	sf::RectangleShape* Particle::SetOrigin(sf::RectangleShape * object)
	{
		object->setOrigin(object->getGlobalBounds().width / 2, object->getGlobalBounds().height / 2);
		return object;
	};
	sf::CircleShape* Particle::SetOrigin(sf::CircleShape * object)
	{
		object->setOrigin(object->getGlobalBounds().width / 2, object->getGlobalBounds().height / 2);
		return object;
	};

	sf::Sprite* Particle::CreateSprite(sf::Texture& texture)
	{
		return SetOrigin(new sf::Sprite(texture));
	};

	sf::RectangleShape* Particle::CreateRectangleShape(const sf::Vector2f& size)
	{
		return SetOrigin(new sf::RectangleShape(size));
	};

	sf::CircleShape* Particle::CreateCircle(const float& radius)
	{
		return SetOrigin(new sf::CircleShape(radius));
	};

	bool Particle::LifetimeLeft(const float& deltatime)
	{
		timer += deltatime;
		return (timer < lifetime);
	};

	void Particle::SetPosition(const sf::Vector2f& pos)
	{
		position = pos;

		if (sprite)
			sprite->setPosition(position);
		if (circle)
			circle->setPosition(position);
		if (rect)
			rect->setPosition(position);
	};

	void Particle::SetRotation(const float& rot)
	{
		rotation = rot;

		if (sprite)
			sprite->setRotation(rotation);
		if (circle)
			circle->setRotation(rotation);
		if (rect)
			rect->setRotation(rotation);
	};

	void Particle::SetDirection(const sf::Vector2f& dir)
	{
		direction = dir;
	};

	void Particle::SetColor(const sf::Color& col) {
		color = col;
		if (sprite)
			sprite->setColor(color);
		if (circle)
			circle->setFillColor(color);
		if (rect)
			rect->setFillColor(color);

	};

	void Particle::SetLifetime(const float& time) {
		lifetime = time;
	};

	void Particle::SetVelocity(const float& speed) {
		velocity = speed;
	};

	void Particle::SetFriction(const float& fric) {
		friction = fric;
	}
	void Particle::SetScale(const float & scale)
	{
		particleScale = scale;
		if (sprite)
			sprite->setScale(particleScale, particleScale);
		if (circle)
			circle->setScale(particleScale, particleScale);
		if (rect)
			rect->setScale(particleScale, particleScale);
	}
	void Particle::UseLocalPosition(const bool & useLocal)
	{
		useLocalPosition = useLocal;
	};

	void Particle::SetColorOverLifetime(const sf::Color& start, const sf::Color& end)
	{
		colorOverLifetime = true;
		startColor = start;
		endColor = end;
	}
	void Particle::SetScaleOverLifetime(const float & start, const float & end)
	{
		scaleOverLifeTime = true;
		startScale = start;
		endScale = end;
	}
	void Particle::SetRotationOverLifetime(const float& amount)
	{
		rotationOverLifetime = amount;
	};

	sf::Vector2f Particle::GetPosition()
	{
		return position;
	};

	sf::Vector2f Particle::GetDirection()
	{
		return direction;
	};

	sf::Color Particle::GetColor()
	{
		return color;
	};

	float Particle::GetLifetime()
	{
		return lifetime;
	};

	float Particle::GetVelocity()
	{
		return velocity;
	};

	float Particle::GetFriction()
	{
		return friction;
	};
};