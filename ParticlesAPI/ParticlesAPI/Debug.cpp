

#include "stdafx.h"


namespace app
{
	Debug::DebugLevel Debug::debugLevel = Debug::DebugLevel::Info;

	Debug::Debug()
	{

	};
	Debug::~Debug()
	{

	}
	void Debug::Log(const DebugLevel & debugLevel, const std::string & text)
	{
		if (ShouldLog(debugLevel))
			std::cout << text << "\n";
	}
	void Debug::Log(const std::string & text)
	{
		std::cout << text << "\n";
	}
	void Debug::SetDebugLevel(const DebugLevel & level)
	{
		debugLevel = level;
	}
	bool Debug::ShouldLog(const DebugLevel & logDebugLevel)
	{
		return (logDebugLevel >= debugLevel);
	};


}