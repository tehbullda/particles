// ParticlesAPI.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <conio.h>
#include "Engine.hpp"


int main()
{
	app::Engine engine;
	if (engine.Initialize())
	{
		while (engine.IsRunning())
		{
			engine.Run();
			engine.Draw();
		}
	}
	engine.CleanUp();


    return 0;
}

